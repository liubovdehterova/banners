const img = document.querySelectorAll('.image-to-show');
const wrapper = document.querySelector('.images-wrapper');
const timerElement = document.getElementById("timer");
let currentIndex = 0;
let timeoutId;
let intervalId;
let countdown = 3000;

//створюємо таймер
function timerDisplay() {
    const seconds = Math.floor(countdown / 1000);
    const milliseconds = countdown % 1000;

    timerElement.textContent = `Час до наступної картинки: ${seconds} с ${milliseconds} мс`;
}

function show() {
    //Перебираємо зображення і додаємо клас active
    img.forEach((item, index) => {
        if(index === currentIndex) {
            item.classList.add('active');
        } else {
            item.classList.remove('active');
        }
    });
    currentIndex = (currentIndex + 1) % img.length; //збільшуємо індекс на одиницю

    //Запускаємо таймер під кажде зображення інтервал кожні 10 мс
    countdown = 3000;//таймер на 3 секунди
    intervalId = setInterval(() => {
        countdown -= 10;//віднімаємо по 10
        timerDisplay();
        if (countdown <= 0) { //перевіряємо чи закінчився таймер
            clearInterval(intervalId);//очищуємо інтервал
            clearTimeout(timeoutId);//очищеємо таймаут
            timeoutId = setTimeout(show, 0);//рекурсія
        }
    }, 10);

    //додаємо кнопки припинити і відновити
    if(!document.querySelector('.btn__reset')) {
        const btnReset = document.createElement("button");
        btnReset.classList.add('btn__reset');
        wrapper.after(btnReset);
        btnReset.textContent = "Відновити показ";
    }

    if(!document.querySelector('.btn')) {
        const btnStop = document.createElement("button");
        btnStop.classList.add('btn');
        wrapper.after(btnStop);
        btnStop.textContent = "Припинити";
    }

}
show();

const stopButton = document.querySelector('.btn');
stopButton.addEventListener('click', () => {
    clearTimeout(timeoutId);
    clearInterval(intervalId);
});


const resumeButton = document.querySelector('.btn__reset');
resumeButton.addEventListener('click', () => {
    clearTimeout(timeoutId);
    clearInterval(intervalId);
    show();
});



